import ReactDOM from 'react-dom/client'
import './components/appcss.css'
import App from './components/App'


const ele = document.querySelector('#root')
const root = ReactDOM.createRoot(ele)

root.render(<App />)