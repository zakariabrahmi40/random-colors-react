import {useEffect, useState} from 'react'

export default function App() {

    const alpha = 'ABCDEF0123456789'
    let text = ''
    const [devs, setDevs] = useState([])


    useEffect(() => {
        for (let i = 0; i < 10; i++) {
                
            setDevs((prevState) => {
                text = ''
                for (let j = 0; j < 6; j++) {
                    text += alpha[Math.floor(Math.random() * alpha.length)]
                }
                return [...prevState, text]
                
            })

        }
    },[])

    function activate() {
        setDevs([])
            for (let i = 0; i < 10; i++) {
                
                setDevs((prevState) => {
                    text = ''
                    for (let j = 0; j < 6; j++) {
                        text += alpha[Math.floor(Math.random() * alpha.length)]
                    }
                    return [...prevState, text]
                    
                })
    
            }
    }
    
   
    function devs_clicked(elm) {
        if (elm.target.innerHTML !== 'COPIED') {
            let cash = elm.target.innerHTML
            elm.target.innerHTML = 'COPIED'
                setTimeout(() => {
                  elm.target.innerHTML = cash
                },1000)

             navigator.clipboard.writeText(cash)
        }

    }


    return(
        <>
            <div style={{ display: 'flex',flexDirection:'column' }}>
                <div className = {'fatherDev'} >
                    {devs.map( (e) => {
                        return <div key={e}  className={'child_div'}>
                                    <div style={{ backgroundColor:e}}></div>
                                    <p onClick={(elm) => devs_clicked(elm)}>#{e}</p>
                            </div>
                    })}
                </div>
                <button className={'btn_ref'} onClick={activate}><i></i><span>Refresh Colors</span><i></i></button>
            </div>
        </>
    )

}